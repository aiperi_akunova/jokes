import './App.css';
import {useEffect, useState} from "react";
import JokesBox from "./components/JokesBox/JokesBox";

const App = () => {
    const [jokes, setJokes] = useState([]);

    const url ='https://api.chucknorris.io/jokes/random';

    useEffect(() => {
        const fetchData = async () => {
            const response = await fetch(url);

            if (response.ok) {
                const joke = await response.json();
                setJokes(joke.value);
            }
        };

        fetchData().catch(e => console.error(e));
    }, []);
    
  return (
      <div className="App">
          <JokesBox message = {jokes}/>
      </div>
  );
};

export default App;
